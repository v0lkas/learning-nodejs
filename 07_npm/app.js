/* jshint esnext: true */

// httpster -p 80 -d ./public

var http = require("http");

http.createServer(function(req,res) {
	res.writeHead(200, {"Content-Type": "text.html"});
	res.end(`<!DOCTYPE html>
<html>
<head>
	<title>Web server</title>
</head>
<body>
	<h1>Web page served</h1>
	<p>You are welsome</p>
</body>
</html>`);
}).listen(80);

console.log("Server running al http://localhost:80");