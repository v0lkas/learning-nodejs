var http = require("http");

var server = http.createServer(function(req, res) {
	res.writeHead(200, {"Content-Type": "text/html"});
	
	res.end(`
	<DOCTYPE html>
	<html lang="EN-us">
		<head>
			<title>Test title</title>
		</head>
		<body>
			<h1>Serving HTML</h1>
			<p>${req.url}</p>
			<p>${req.method}</p>
		</body>
	</html>`);
});

server.listen(80);

console.log("Server listening on port 80");