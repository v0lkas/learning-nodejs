var http = require("http");

var data = require("./lib/inventory.json");

http.createServer(function(req, res) {
	
	if (req.url === "/") {
		res.writeHead(200, {"Content-type": "text/json"});
		res.end(JSON.stringify(data));
	} else if (req.url === "/instock") {
		listInStock(res);
	} else if (req.url === "/onorder") {
		listOnBackOrder(res);
	} else {
		res.wroteHead(404, {"Content-Type": "text/plain"});
		res.end("Whoops...");
	}
}).listen(80);

console.log("Server is online");

function listInStock(res) {
	var inStock = data.filter(function(item) {
		return item.avail === "In stock";
	});
	
	res.end(JSON.stringify(inStock));
}

function listOnBackOrder(res) {
	var onOrder = data.filter(function(item) {
		return item.avail === "On back order";
	});
	
	res.end(JSON.stringify(onOrder));
}