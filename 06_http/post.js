var http = require("http");
var fs = require("fs");
var path = require("path");

http.createServer(function(req, res) {
	if (req.url === "/") {
		if (req.method === "GET") {
			res.writeHead(200, {"Content-Type": "text/html"});
			fs.createReadStream("./public/index.html", "UTF-8").pipe(res);
		} else if (req.method === "POST") {
			var body = "";
			req.on("data", function(chunk) {
				body += chunk;
			});
			req.on("end", function() {
				res.writeHead(200, {"Content-Type": "text/html"});
				res.end(`<!DOCTYPE html>
<html>
	<head>
		<title>POST info</title>
	</head>
	<body>
		<h1>Your form results</h1>
		<p>${body}</p>
	</body>
</html>`);
			});
		}
	}else if (req.url.match(/.css$/)) {
		var cssPath = path.join(__dirname, 'public', req.url);
		var fileStream = fs.createReadStream(cssPath, "UTF-8");
		
		res.writeHead(200, {"Content-Type": "text/css"});
		fileStream.pipe(res);
	}else if (req.url.match(/.jpg$/)) {
		var imgPath = path.join(__dirname, 'public', req.url);
		var imgStream = fs.createReadStream(imgPath);
		
		res.writeHead(200, {"Content-Type": "image/jpeg"});
		imgStream.pipe(res);
	} else {
		res.writeHead(404, {"Content-Type": "text/plain"});
		res.end("404 File not found");
	}
}).listen(80);

console.log("Server is online");