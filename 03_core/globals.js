// global.console.log("Hye World\n");
var hello = "Hello World from Node js";
var justNode = hello.slice(17);
console.log(`Rock on World from ${justNode}\n`);

console.log("DIRNAME:  "+__dirname);
console.log("FILENAME: "+__filename);


console.log('\n');



// Working with paths
var path = require("path");
console.log(`Rock on World from ${path.basename(__filename)}\n`);