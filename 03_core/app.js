function grab(flag) {
	var index = process.argv.indexOf(flag);
	return (index === -1) ? null : process.argv[index+1];
}

var greeting = grab('--greeting');
var user = grab('--name');

if (!user || !greeting) {
	console.log("Use command \"node greeting --name NAME --greeting GREETING\"!");
} else {
	console.log(`Welcome ${user}, ${greeting}`);
}