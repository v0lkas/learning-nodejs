var fs = require("fs");

// fs.rmdir("./sample_dir_moved", function(err) {
	// if(err) {
		// throw err;
	// }

	// console.log("First directory removed");
// });

fs.readdirSync("./lib/logs").forEach(function(fileName) {
	var stats = fs.statSync("./lib/logs/"+fileName);
	if(stats.isFile()) {
		fs.unlinkSync("./lib/logs/"+fileName);
	} else if(stats.isDirectory()) {
		fs.rmdirSync("./lib/logs/"+fileName);
	}
	//
});

fs.rmdir("./lib/logs", function(err) {
	if(err) {
		throw err;
	}

	console.log("Directory and it content has been removed");
});