var fs = require("fs");

if(fs.existsSync("lib/sample_dir")) {
	console.log("Directory already exists");
} else {
	fs.mkdir("lib/sample_dir", function(err) {
		if(err) {
			console.log(err);
		} else {
			console.log("Directory created");
		}
	});
}