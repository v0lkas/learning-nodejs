// Modules require
var path = require('path');
var util = require('util');
var v8 = require('v8');



// path module testing
var dirUploads = path.join(__dirname, 'www', 'files', 'uploads');
console.log(`PATH FILENAME: ${path.basename(__filename)}\n`);
console.log(`PATH TEST FOLDER: ${dirUploads}\n`);



// util module testing
util.log(`UTIL FILENAME: ${path.basename(__filename)}\n`);
util.log(`UTIL TEST FOLDER: ${dirUploads}\n`);



// v8 module testing
util.log(v8.getHeapStatistics());